const INDEX = "index"
const CONTACT = "contact"
const JOKE_PAGE = "joke"
const JOB_SCOPES = "job-scopes"

export const PAGE_NAMES = Object.freeze({
  INDEX: INDEX,
  CONTACT: CONTACT,
  JOKE_PAGE: JOKE_PAGE,
  JOB_SCOPES: JOB_SCOPES,
})

export const UNSPLASH_COM_DAILY_URL = "https://source.unsplash.com/daily"

export const GET_WIKIPEDIA_REST_API_URL = (lang: string = "en") =>
  `https://${lang && lang}.wikipedia.org/api/rest_v1/page/summary`

export const REPOSITORY_URL = "https://gitlab.com/atcheri/nodeat.online"

export const RESUME_URL = "https://www.visualcv.com/pdfs/2464670/"

export const LANGUAGE = Object.freeze({
  ENGLISH: "en",
  FRENCH: "fr",
  GERMAN: "de",
  JAPANESE: "ja",
})
