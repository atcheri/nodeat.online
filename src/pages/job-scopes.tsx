import React from "react"
import { Container } from "@material-ui/core"

import ToContactOrJoke from "./job-scopes/to-contact-or-joke"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { JobScopesContextProvider } from "../components/job-scopes-module/job-scopes-context"
import JobScopesModule from "../components/job-scopes-module"

import "./job-scopes/job-scope-layout.scss"

const JobScopes = (/*{ path, location }: RouteComponentProps*/) => {
  return (
    <Layout>
      <SEO title="Technology stacks by work scope" />
      <JobScopesContextProvider>
        <Container fixed data-testid="job-scopes-container">
          <JobScopesModule />
          <ToContactOrJoke />
        </Container>
      </JobScopesContextProvider>
    </Layout>
  )
}

export default JobScopes
