import React from "react"
import { IntlProvider } from "react-intl"
import { IntlContextProvider } from "gatsby-plugin-intl"
import { render } from "@testing-library/react"
import { useStaticQuery } from "gatsby"
import Contact from "../contact"
import { LANGUAGE } from "../../constants"

jest.mock("gatsby")
jest.mock("gatsby-plugin-transition-link/AniLink", () => {
  return () => <div>Mock</div>
})

const mockedUseStaticQuery = useStaticQuery as jest.Mock<typeof useStaticQuery>

describe("<Contact />", () => {
  it("renders", () => {
    mockedUseStaticQuery.mockReturnValue({
      site: {
        siteMetadata: { title: "title test" },
      },
      file: {
        childImageSharp: {
          fixed: { src: "test-logo.png" },
        },
      },
    } as any)
    const { getByTestId } = render(
      <IntlContextProvider
        value={{
          languages: Object.values(LANGUAGE),
          language: "en",
          routed: true,
        }}
      >
        <IntlProvider locale={LANGUAGE.ENGLISH} onError={() => null}>
          <Contact />
        </IntlProvider>
      </IntlContextProvider>
    )
    getByTestId("contact-form")
  })
})
