import React from "react"
import { render } from "@testing-library/react"
import { useStaticQuery } from "gatsby"
import { IntlContextProvider } from "gatsby-plugin-intl"
import { IntlProvider } from "react-intl"

import JokePage from "../joke"
import { LANGUAGE } from "../../constants"

jest.mock("gatsby")
jest.mock("gatsby-plugin-transition-link/AniLink", () => {
  return ({ children }) => <>{children}</>
})

const mockedUseStaticQuery = useStaticQuery as jest.Mock<typeof useStaticQuery>

describe("<JokePage />", () => {
  it("should render page", () => {
    mockedUseStaticQuery.mockReturnValue({
      site: {
        siteMetadata: { title: "title test" },
      },
      file: {
        childImageSharp: {
          fixed: { src: "test-logo.png" },
        },
      },
    } as any)
    const { getByTestId } = render(
      <IntlContextProvider
        value={{
          languages: Object.values(LANGUAGE),
          language: LANGUAGE.ENGLISH,
          routed: true,
        }}
      >
        <IntlProvider locale={LANGUAGE.ENGLISH} onError={() => null}>
          <JokePage />
        </IntlProvider>
      </IntlContextProvider>
    )
    getByTestId("joke-search")
  })
})
