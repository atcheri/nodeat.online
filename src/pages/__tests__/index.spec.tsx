import React from "react"
import { render, fireEvent, wait } from "@testing-library/react"
import { IntlProvider } from "react-intl"

import { IndexPage, HOME_PAGE } from "../index"
import { LANGUAGE } from "../../constants"

jest.mock("gatsby-plugin-transition-link/AniLink", () => {
  return ({ children }) => <>{children}</>
})

const renderIndexPage = () =>
  render(
    <IntlProvider locale={LANGUAGE.ENGLISH} onError={() => null}>
      <IndexPage data={{ file: { childImageSharp: { fixed: { src: "" } } } }} />
    </IntlProvider>
  )

describe("<IndexPage />", () => {
  it("should render page", () => {
    const { getByTestId } = renderIndexPage()
    getByTestId(`${HOME_PAGE}-container`)
  })

  describe("When the logo image is hovered", () => {
    it("displays the Enter button", async () => {
      const { queryByTestId, getByTestId } = renderIndexPage()
      expect(queryByTestId(`${HOME_PAGE}-enter-button`)).toBeNull()
      const logo = getByTestId(`${HOME_PAGE}-logo`)
      fireEvent.mouseOver(logo)
      const btn = await wait(() => getByTestId(`${HOME_PAGE}-enter-button`))
    })
  })
})
