import React from "react"
import { render } from "@testing-library/react"
import { useStaticQuery } from "gatsby"
import { IntlContextProvider } from "gatsby-plugin-intl/intl-context"
import { IntlProvider } from "react-intl"

import JobScopes from "../job-scopes"
import { LANGUAGE } from "../../constants"

jest.mock("gatsby")
jest.mock("gatsby-plugin-transition-link/AniLink", () => {
  return ({ children }) => <>{children}</>
})
jest.mock("../../hooks/useOnScreen", () => {
  return () => false
})

const mockedUseStaticQuery = useStaticQuery as jest.Mock<typeof useStaticQuery>

describe("<JobScopes />", () => {
  it("should render page", () => {
    mockedUseStaticQuery.mockReturnValue({
      site: {
        siteMetadata: { title: "title test" },
      },
      file: {
        childImageSharp: {
          fixed: { src: "test-logo.png" },
        },
      },
    } as any)
    const { getByTestId } = render(
      <IntlContextProvider
        value={{
          languages: Object.values(LANGUAGE),
          language: "en",
          routed: true,
        }}
      >
        <IntlProvider locale={LANGUAGE.ENGLISH} onError={() => null}>
          <JobScopes />
        </IntlProvider>
      </IntlContextProvider>
    )
    getByTestId("job-scopes-container")
  })
})
