import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import ContactForm from "../components/contact-form"
import CenteredGrid from "../components/centered-grid"

const Contact = () => {
  return (
    <Layout>
      <SEO title="Contact me" />
      <CenteredGrid>
        <ContactForm />
      </CenteredGrid>
    </Layout>
  )
}

export default Contact
