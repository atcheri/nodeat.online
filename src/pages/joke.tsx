import React from "react"
import Grid from "@material-ui/core/Grid"

import CenteredGrid from "../components/centered-grid"
import JokeSearch from "../components/joke-search"
import Layout from "../components/layout"

const JokePage = () => {
  return (
    <Layout>
      <CenteredGrid>
        <Grid item data-testid="joke-search">
          <JokeSearch />
        </Grid>
      </CenteredGrid>
    </Layout>
  )
}

export default JokePage
