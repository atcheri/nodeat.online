import React, { useRef } from "react"
import { Box } from "@material-ui/core"
import AniLink from "gatsby-plugin-transition-link/AniLink"
import Grow from "@material-ui/core/Grow"
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles"
import { useIntl } from "gatsby-plugin-intl"

import useOnScreen from "../../hooks/useOnScreen"
import { PAGE_NAMES } from "../../constants"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    wrapper: {
      padding: theme.spacing(5),
    },
    bottom: {
      paddingTop: "1rem",
    },
  })
)

const ToContactOrJoke = () => {
  const ref = useRef()
  const intl = useIntl()

  const visible = useOnScreen(ref)

  const classes = useStyles({})
  return (
    <div ref={ref} className={classes.wrapper}>
      <Grow in={visible} timeout={1000}>
        <Box className={classes.bottom}>
          {intl.formatMessage({
            id: "job-scope.to-contact-or-joke.anything-interesting",
          })}{" "}
          <AniLink
            cover
            to={`/${PAGE_NAMES.CONTACT}`}
            direction="left"
            bg="#34cc99"
          >
            {intl.formatMessage({
              id: "job-scope.to-contact-or-joke.contact-me",
            })}
          </AniLink>
        </Box>
      </Grow>
      <Grow in={visible} {...(visible ? { timeout: 2000 } : {})}>
        <Box className={classes.bottom}>
          {intl.formatMessage({
            id: "job-scope.to-contact-or-joke.too-much-info",
          })}{" "}
          <AniLink
            cover
            to={`/${PAGE_NAMES.JOKE_PAGE}`}
            direction="left"
            bg="#34cc99"
          >
            <i>
              {intl.formatMessage({
                id: "job-scope.to-contact-or-joke.dad-joke",
              })}
            </i>
          </AniLink>
        </Box>
      </Grow>
    </div>
  )
}

export default ToContactOrJoke
