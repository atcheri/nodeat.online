import React, { useState } from "react"
import { useStaticQuery, graphql } from "gatsby"
import AniLink from "gatsby-plugin-transition-link/AniLink"
import { Button, Grid } from "@material-ui/core"
import { useIntl } from "gatsby-plugin-intl"

import SEO from "../components/seo"
import { PAGE_NAMES } from "../constants"

export const HOME_PAGE = "home-page"

export const IndexPage = ({ data }) => {
  const [show, setShow] = useState(false)
  const intl = useIntl()

  const jobScopePath = `/${PAGE_NAMES.JOB_SCOPES}`

  return (
    <Grid
      container
      style={{ minHeight: "100vh" }}
      spacing={0}
      alignContent="center"
      justify="center"
      direction="column"
    >
      <Grid
        container
        item
        alignContent="center"
        justify="center"
        data-testid={`${HOME_PAGE}-container`}
      >
        <AniLink cover bg="#34cc99" to={jobScopePath}>
          <img
            src={data.file.childImageSharp.fixed.src}
            alt="AE technology stack"
            data-testid={`${HOME_PAGE}-logo`}
            onMouseOver={() => setShow(true)}
          />
        </AniLink>
      </Grid>
      <div style={{ minHeight: "39px" }}>
        {show && (
          <Grid container item alignContent="center" justify="center">
            <AniLink cover bg="#34cc99" to={jobScopePath}>
              <Button
                size="medium"
                color="primary"
                data-testid={`${HOME_PAGE}-enter-button`}
              >
                {intl.formatMessage({ id: "index.main_link" })}
              </Button>
            </AniLink>
          </Grid>
        )}
      </div>
    </Grid>
  )
}

const IndexPageWithQuery = () => {
  const data = useStaticQuery(graphql`
    query {
      file(relativePath: { eq: "ae-logo.png" }) {
        childImageSharp {
          fixed(width: 280, height: 280) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)
  return (
    <>
      <SEO title="Home" />
      <IndexPage data={data} />
    </>
  )
}

export default IndexPageWithQuery
