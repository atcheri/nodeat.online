import translate from "translate"

import { LANGUAGE } from "../constants"

const engine = "yandex"
const key =
  "trnsl.1.1.20200110T201520Z.c345708b0345bc96.3409b9921c846c2198bcbb2f6e3a796ac7ed409e"

export const translateText = async (
  text: string,
  from: string = LANGUAGE.ENGLISH,
  to: string = LANGUAGE.ENGLISH
): Promise<string> => {
  const translation = await translate(text, { from, to, engine, key })
  console.log("translation:", translation)
  return translation
}
