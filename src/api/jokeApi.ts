export const JOKE_API_URL = "https://icanhazdadjoke.com/"

export interface IJoke {
  id: string
  joke: string
  status: number
}

class JokeApiError extends Error {}

export const fetchFromJokeApi = async (
  search: string,
  limit = 5
): Promise<IJoke[]> => {
  try {
    const resp = await fetch(
      `${JOKE_API_URL}/search?term=${search}&limit=${limit}`,
      {
        headers: {
          Accept: "application/json",
        },
      }
    )
    const { results } = await resp.json()
    return results as IJoke[]
  } catch (err) {
    throw new JokeApiError(err)
  }
}
