interface IGetFormPayload {
  name: string
  email: string
  phone: string
  message: string
}

export const sendToMailerApi = async (
  payload: IGetFormPayload
): Promise<boolean> => {
  try {
    const response = await await fetch(
      "https://getform.io/f/74924847-9246-4364-8d90-0ffe00db21c3",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
      }
    )
    return response.status === 200
  } catch (err) {
    throw new Error(err)
  }
}
