import React, { useState, useCallback, useMemo } from "react"
import AniLink from "gatsby-plugin-transition-link/AniLink"
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles"
import {
  Button,
  Paper,
  InputBase,
  IconButton,
  Typography,
} from "@material-ui/core"
import { green } from "@material-ui/core/colors"
import MoodIcon from "@material-ui/icons/Mood"
import MoodBadIcon from "@material-ui/icons/MoodBad"
import SearchIcon from "@material-ui/icons/Search"
import KeyboardArrowRightIcon from "@material-ui/icons/KeyboardArrowRight"
import { useIntl } from "gatsby-plugin-intl"

import { generateZeroOrOne } from "./joke-search/helpers"
import JokesList from "./joke-search/jokes-list"
import NoJokeFound from "./joke-search/node-joke-found"
import { fetchFromJokeApi, IJoke, JOKE_API_URL } from "../api/jokeApi"
import { PAGE_NAMES } from "../constants"

export interface IJokeSearchProps {
  "data-testid"?: string
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: "2px 4px",
      display: "flex",
      alignItems: "center",
      width: 400,
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
  })
)

const emptyJokes: IJoke[] = []

const JokeSearch = (props: IJokeSearchProps) => {
  const [zeroOrOne, setZeroOrOne] = useState(Math.floor(Math.random()))
  const [searchValue, setSearchValue] = useState("")
  const [searched, setSearched] = useState(false)
  const [jokes, setJokes] = useState(emptyJokes)
  const intl = useIntl()
  const placeHolder = useMemo(
    () => intl.formatMessage({ id: "joke.input_placeholder" }),
    [intl.locale]
  )

  const updateIcon = useCallback(() => {
    const random = generateZeroOrOne()
    setZeroOrOne(random)
  }, [])

  const callJokeApiAndSetJoke = async (search: string) => {
    try {
      const fetchedJoke = await fetchFromJokeApi(search)
      setJokes(fetchedJoke)
      setSearched(true)
    } catch (err) {
      console.log(err)
    }
  }

  const classes = useStyles({})

  return (
    <>
      <Paper
        component="form"
        className={classes.root}
        onSubmit={e => {
          e.preventDefault()
          callJokeApiAndSetJoke(searchValue)
        }}
      >
        <IconButton
          data-testid="mood-icon"
          className={classes.iconButton}
          aria-label="menu"
          onClick={updateIcon}
        >
          {zeroOrOne ? (
            <MoodIcon style={{ color: green[500] }} />
          ) : (
            <MoodBadIcon color="disabled" />
          )}
        </IconButton>
        <InputBase
          inputProps={{
            "data-testid": "joke-search-input",
            "aria-label": placeHolder.toLowerCase(),
          }}
          className={classes.input}
          placeholder={placeHolder}
          value={searchValue}
          onChange={e => setSearchValue(e.target.value)}
        />
        <IconButton
          data-testid="joke-search-button"
          type="submit"
          className={classes.iconButton}
          aria-label="search"
          disabled={!searchValue}
        >
          <SearchIcon />
        </IconButton>
      </Paper>
      <Typography component="p" variant="body2" color="textSecondary">
        {intl.formatMessage({ id: "joke.input_hint" })}{" "}
        <a href={JOKE_API_URL} target="_blank" rel="nofollow">
          ({JOKE_API_URL})
        </a>
      </Typography>
      {jokes.length === 0 ? (
        <NoJokeFound searched={searched} searchValue={searchValue} />
      ) : (
        <>
          <JokesList jokes={jokes} />
          <Typography
            component="div"
            variant="body2"
            color="textPrimary"
            style={{ marginBottom: "1rem" }}
          >
            {intl.formatMessage({ id: "joke.last_message" })}
          </Typography>
          <AniLink
            cover
            bg="#34cc99"
            to={`/${PAGE_NAMES.JOB_SCOPES}`}
            style={{ textDecoration: "none" }}
          >
            <Button variant="outlined" size="medium" color="primary">
              {intl.formatMessage({ id: "joke.go_to_next" })}
              <KeyboardArrowRightIcon />
            </Button>
          </AniLink>
        </>
      )}
    </>
  )
}

export default JokeSearch
