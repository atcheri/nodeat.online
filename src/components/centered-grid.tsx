import React, { FunctionComponent } from "react"
import { Grid } from "@material-ui/core"

const CenteredGrid: FunctionComponent = ({ children }) => {
  return (
    <Grid
      style={{ minHeight: "85vh" }}
      container
      spacing={0}
      alignContent="center"
      justify="center"
      direction="column"
    >
      {children}
    </Grid>
  )
}

export default CenteredGrid
