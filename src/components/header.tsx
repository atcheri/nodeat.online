import React, { FunctionComponent } from "react"
import { graphql, useStaticQuery } from "gatsby"
import PropTypes from "prop-types"
import AniLink from "gatsby-plugin-transition-link/AniLink"
import {
  AppBar,
  Avatar,
  Toolbar,
  Typography,
  IconButton,
} from "@material-ui/core"
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles"
import AlternateEmailIcon from "@material-ui/icons/AlternateEmail"
import CloudDownloadOutlinedIcon from "@material-ui/icons/CloudDownloadOutlined"
import LanguageSelect from "./header/language-select"
import { PAGE_NAMES, RESUME_URL } from "../constants"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    header: {
      flexGrow: 1,
      marginBottom: theme.spacing(1),
    },
    logo: {
      backgroundColor: "white",
      marginRight: theme.spacing(2),
    },
    siteTitle: {
      [theme.breakpoints.down("xs")]: {
        display: "none",
      },
    },
    grow: {
      flexGrow: 1,
    },
    iconButton: {
      color: "inherit",
    },
  })
)

interface IHeaderLink {
  to?: string
  direction?: string
  className?: any
}

const HeaderLink: FunctionComponent<IHeaderLink> = ({
  children,
  to = "/",
  direction = "right",
  className = {},
}) => {
  return (
    <AniLink
      cover
      direction={direction}
      bg="#34cc99"
      to={to}
      style={{
        color: `white`,
        textDecoration: `none`,
      }}
      className={className}
    >
      {children}
    </AniLink>
  )
}

const Header = ({ siteTitle }) => {
  const data = useStaticQuery(graphql`
    query {
      file(relativePath: { eq: "ae-logo.png" }) {
        childImageSharp {
          fixed(width: 50, height: 50) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)

  const classes = useStyles({})
  return (
    <header className={classes.header}>
      <AppBar position="static">
        <Toolbar variant="dense">
          <HeaderLink>
            <Avatar
              className={classes.logo}
              alt="nodeat.online logo"
              src={data.file.childImageSharp.fixed.src}
            />
          </HeaderLink>
          <Typography
            variant="body1"
            component="h1"
            className={classes.siteTitle}
          >
            <HeaderLink>{siteTitle}</HeaderLink>
          </Typography>
          <div className={classes.grow}></div>
          <LanguageSelect />
          <IconButton color="inherit" target="_blank" href={RESUME_URL}>
            <CloudDownloadOutlinedIcon />
          </IconButton>
          <HeaderLink
            to={`/${PAGE_NAMES.CONTACT}`}
            direction="left"
            className={classes.iconButton}
          >
            <IconButton color="inherit">
              <AlternateEmailIcon />
            </IconButton>
          </HeaderLink>
        </Toolbar>
      </AppBar>
    </header>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
