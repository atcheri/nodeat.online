import React, { useState } from "react"
import { Button, Menu, MenuItem } from "@material-ui/core"
import { IntlContextConsumer, changeLocale } from "gatsby-plugin-intl"
import TranslateIcon from "@material-ui/icons/Translate"
import ExpandMoreOutlinedIcon from "@material-ui/icons/ExpandMoreOutlined"

import { languages as languagesMap, getLanguageName } from "./helpers"

const LanguageSelect = () => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)

  const handleMenuItemClick = (code: string) => {
    changeLocale(code)
    setAnchorEl(null)
  }

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <IntlContextConsumer>
      {({ languages, language: currentLocale }) => {
        return (
          <>
            <Button
              aria-label="choose your language"
              aria-controls="customized-menu"
              aria-haspopup="true"
              onClick={handleClick}
              color="inherit"
              startIcon={<TranslateIcon />}
              endIcon={<ExpandMoreOutlinedIcon />}
            >
              {getLanguageName(currentLocale)}
            </Button>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              {languages.map((l, _) => {
                const lang = languagesMap.find(e => e.code === l)
                return (
                  <MenuItem
                    key={lang.code}
                    disabled={lang.code === currentLocale}
                    selected={lang.code === currentLocale}
                    onClick={() => handleMenuItemClick(lang.code)}
                  >
                    {lang.name}
                  </MenuItem>
                )
              })}
            </Menu>
          </>
        )
      }}
    </IntlContextConsumer>
  )
}

export default LanguageSelect
