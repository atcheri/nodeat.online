import { LANGUAGE } from "../../constants"

interface ILanguage {
  code: string
  name: string
}

export const languages: readonly ILanguage[] = Object.freeze([
  {
    code: LANGUAGE.ENGLISH,
    name: "English",
  },
  {
    code: LANGUAGE.FRENCH,
    name: "Français",
  },
  {
    code: LANGUAGE.GERMAN,
    name: "Deutsch",
  },
  {
    code: LANGUAGE.JAPANESE,
    name: "日本語",
  },
])

export const getLanguageName = (code: string) => {
  const language = languages.find(l => l.code === code)
  return language ? language.name : "English"
}
