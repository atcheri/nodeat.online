import React from "react"
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles"
import { Box, IconButton, Tooltip } from "@material-ui/core"

import { REPOSITORY_URL } from "../constants"
import GitlabIcon from "./icons/GitlabIcon"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    footer: {
      paddingTop: "1rem",
      position: "fixed",
      width: "100%",
      top: "auto",
      bottom: 0,
    },
    bottom: {
      backgroundColor: "#ffffff",
      textAlign: "center",
      padding: "0.1rem 0.1rem",
      [theme.breakpoints.down("xs")]: {
        display: "none",
      },
    },
  })
)

const Footer = () => {
  const classes = useStyles({})

  return (
    <footer className={classes.footer}>
      <Box
        component="div"
        className={classes.bottom}
        data-testid="footer-bottom"
      >
        <Box component="div">
          <a href="#">
            © {new Date().getFullYear()},{" "}
            {process.env.SITE_URL || "nodeat.online"}
          </a>
        </Box>
        <Box component="div">
          <Tooltip title="Repository on gitlab.com">
            <a href={REPOSITORY_URL} target="_blank" rel="nofollow">
              <IconButton aria-label="delete" size="small">
                <GitlabIcon />
              </IconButton>{" "}
            </a>
          </Tooltip>
        </Box>
      </Box>
    </footer>
  )
}

export default Footer
