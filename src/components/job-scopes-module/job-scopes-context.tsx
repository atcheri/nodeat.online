import React, { useState } from "react"

const JobScopesContext = React.createContext(null)

export const JobScopesContextProvider = ({ children }) => {
  const [expanded, setExpanded] = useState(false)
  const [scopeSlug, setScopeSlug] = useState(null)

  const value = {
    expanded,
    setExpanded,
    scopeSlug,
    setScopeSlug,
  }
  return (
    <JobScopesContext.Provider value={value}>
      {children}
    </JobScopesContext.Provider>
  )
}

export const JobScopesContextConsumer = JobScopesContext.Consumer

export default JobScopesContext
