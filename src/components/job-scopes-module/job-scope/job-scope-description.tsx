import React from "react"
import {
  Avatar,
  Card,
  CardHeader,
  CardContent,
  Typography,
} from "@material-ui/core"
import DescriptionIcon from "@material-ui/icons/Description"
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles"
import { purple } from "@material-ui/core/colors"

import { IJobScope } from "../job-scopes-list/interfaces"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    avatar: {
      backgroundColor: purple[500],
    },
  })
)

interface IJobScopeDescription {
  scope: IJobScope
}

const JobScopeDescription = ({ scope }: IJobScopeDescription) => {
  const classes = useStyles({})

  return (
    <Card>
      <CardHeader
        avatar={
          <Avatar className={classes.avatar}>
            <DescriptionIcon />
          </Avatar>
        }
        title="Description"
        subheader="Assigned role and accomplished features"
      />
      <CardContent>
        <Typography variant="body2" component="p">
          {scope.description}
        </Typography>
      </CardContent>
    </Card>
  )
}

export default JobScopeDescription
