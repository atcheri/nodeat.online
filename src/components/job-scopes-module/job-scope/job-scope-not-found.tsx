import React from "react"
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles"
import { Paper, Typography, Button } from "@material-ui/core"
import { Flipped } from "react-flip-toolkit"

import useJobScope from "../hooks/useJobScope"
import { FLIP_ID } from "../../job-scopes-module"

interface IJobScopeNotFound {
  slug: string
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: theme.spacing(3, 2),
    },
    content: {
      padding: theme.spacing(3, 2),
    },
  })
)

const JobScopeNotFound = ({ slug }: IJobScopeNotFound) => {
  const { expandScope } = useJobScope()
  const classes = useStyles({})
  return (
    <Flipped flipId={FLIP_ID}>
      <Paper className={classes.root} data-testid="job-scope-not-found">
        <Typography variant="h5" component="h3">
          No data found for this job scope "{slug}"
        </Typography>
        <Typography
          className={classes.content}
          variant="body2"
          component="p"
          color="textSecondary"
        >
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae
          velit illo expedita cupiditate, officia ipsa, at dolores
          exercitationem, voluptatem quibusdam nemo repellendus. Placeat, fugiat
          temporibus? Animi ea ipsa eum accusamus!
        </Typography>
        <Button
          variant="outlined"
          color="primary"
          onClick={() => expandScope(null)}
        >
          Back
        </Button>
      </Paper>
    </Flipped>
  )
}

export default JobScopeNotFound
