import React, { FunctionComponent } from "react"
import {
  Avatar,
  Button,
  Card,
  CardContent,
  CardHeader,
  Typography,
} from "@material-ui/core"
import BusinessIcon from "@material-ui/icons/Business"
import LaunchIcon from "@material-ui/icons/Launch"
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles"
import { blue } from "@material-ui/core/colors"
import { useIntl, FormattedHTMLMessage } from "gatsby-plugin-intl"

import { IJobScope } from "../job-scopes-list/interfaces"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    avatar: {
      backgroundColor: blue[500],
    },
  })
)

interface IJobScopeCompany {
  scope: IJobScope
}

const JobScopeCompany: FunctionComponent<IJobScopeCompany> = ({
  scope: { slug, company },
}) => {
  const classes = useStyles({})
  const intl = useIntl()

  return (
    <Card>
      <CardHeader
        avatar={
          <Avatar className={classes.avatar}>
            <BusinessIcon />
          </Avatar>
        }
        action={
          company.url && (
            <Button
              href={company.url}
              color="primary"
              variant="outlined"
              target="_blank"
            >
              Site
              <LaunchIcon />
            </Button>
          )
        }
        title={company.name}
        subheader={intl.formatMessage({
          id: `constants.countries.${company.location.country}.name`,
        })}
      />
      <CardContent>
        <Typography variant="body2" component="p">
          <FormattedHTMLMessage id={`jobs.${slug}.company.description`} />
        </Typography>
      </CardContent>
    </Card>
  )
}

export default JobScopeCompany
