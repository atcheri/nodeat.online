import React, { useEffect, useRef } from "react"
import { Grid, Icon } from "@material-ui/core"
import { spring } from "react-flip-toolkit"

import { ITechnology } from "../job-scopes-list/interfaces"

import "./job-scope-technologies.scss"

interface IJobScopeTechnologies {
  technologies: ITechnology[]
}

const JobScopeTechnologies = ({ technologies = [] }: IJobScopeTechnologies) => {
  const containerRef = useRef(null)

  useEffect(() => {
    const items = [
      ...containerRef.current.querySelectorAll(".techonology-item"),
    ]
    items.forEach((el, i) => {
      spring({
        config: "wobbly",
        values: {
          translateY: [20, 0],
          opacity: [0, 1],
        },
        onUpdate: ({
          translateY,
          opacity,
        }: {
          translateY: number
          opacity: number
        }) => {
          el.style.opacity = opacity
          el.style.transform = `translateY(${translateY}px)`
        },
        delay: i * 250,
      })
    })
  }, [])

  return (
    <Grid container spacing={3} ref={containerRef}>
      {technologies.map((tech, i) => (
        <Grid item xs={6} sm={2} key={`tech-${i}`} className="techonology-item">
          {tech.icon.component ? (
            <tech.icon.component />
          ) : (
            <Icon className={`devicon-${tech.icon.name} colored`} />
          )}
          <div className="capitalize">{tech.title}</div>
          {tech.description && <div>{tech.description}</div>}
          {tech.repository && (
            <div>
              <a href={tech.repository}>Site</a>
            </div>
          )}
        </Grid>
      ))}
    </Grid>
  )
}

export default JobScopeTechnologies
