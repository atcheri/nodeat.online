import React from "react"
import {
  Icon,
  List,
  ListItem,
  Avatar,
  ListItemText,
  ListItemIcon,
  Card,
  CardHeader,
  CardContent,
} from "@material-ui/core"
import { Rating } from "@material-ui/lab"
import StarBorderIcon from "@material-ui/icons/StarBorder"
import KeyboardIcon from "@material-ui/icons/Keyboard"
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles"
import { teal } from "@material-ui/core/colors"

import { ITechnology } from "../job-scopes-list/interfaces"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    avatar: {
      backgroundColor: teal[500],
    },
  })
)

interface IJobScopeTechnologiesList {
  technologies: ITechnology[]
}

const JobScopeTechnologiesList = ({
  technologies = [],
}: IJobScopeTechnologiesList) => {
  const classes = useStyles({})

  return (
    <Card>
      <CardHeader
        avatar={
          <Avatar className={classes.avatar}>
            <KeyboardIcon />
          </Avatar>
        }
        title="Technologies"
        subheader="Used tech stack"
      />
      <CardContent>
        <List>
          {technologies.map((tech, i) => (
            <ListItem key={`tech-${i}`}>
              <ListItemIcon>
                {tech.icon.component ? (
                  <tech.icon.component />
                ) : (
                  <Icon className={`devicon-${tech.icon.name} colored`} />
                )}
              </ListItemIcon>
              <ListItemText
                primary={tech.title}
                secondary={
                  <Rating
                    value={tech.level}
                    precision={0.5}
                    size="small"
                    emptyIcon={<StarBorderIcon fontSize="inherit" />}
                    readOnly
                  />
                }
              />
            </ListItem>
            // <div className="capitalize">{tech.title}</div>
            // {tech.description && <div>{tech.description}</div>}
            // {tech.repository && (
            //   <div>
            //     <a href={tech.repository}>Site</a>
            //   </div>
          ))}
        </List>
      </CardContent>
    </Card>
  )
}

export default JobScopeTechnologiesList
