import React, { FunctionComponent } from "react"
import { Avatar, Card, CardHeader } from "@material-ui/core"
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles"
import { blueGrey } from "@material-ui/core/colors"
import DateRangeIcon from "@material-ui/icons/DateRange"
import { useIntl } from "gatsby-plugin-intl"

import { IJobScope } from "../job-scopes-list/interfaces"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    avatar: {
      backgroundColor: blueGrey[500],
    },
  })
)

interface IJobScopeDates {
  scope: IJobScope
}

const JobScopeDates: FunctionComponent<IJobScopeDates> = ({ scope }) => {
  const classes = useStyles({})
  const intl = useIntl()

  return (
    <Card>
      <CardHeader
        avatar={
          <Avatar className={classes.avatar}>
            <DateRangeIcon />
          </Avatar>
        }
        title={
          <>
            {scope.start.getFullYear()}
            {" ~ "}
            {scope.end
              ? scope.end.getFullYear()
              : intl.formatMessage({ id: "constants.times.today" })}
          </>
        }
      />
    </Card>
  )
}

export default JobScopeDates
