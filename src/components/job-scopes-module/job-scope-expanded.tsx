import React, { useEffect, useRef } from "react"
import { Button, Grid, Typography } from "@material-ui/core"
import KeyboardReturnIcon from "@material-ui/icons/KeyboardReturn"
import { Flipped } from "react-flip-toolkit"
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles"
import { useIntl } from "gatsby-plugin-intl"

import { getJobScope } from "./job-scopes-list/constants"
import JobScopeNotFound from "./job-scope/job-scope-not-found"
import useJobScope from "./hooks/useJobScope"
import { FLIP_ID } from "../job-scopes-module"
import JobScopeCompany from "./job-scope/job-scope-company"
import JobScopeTechnologiesList from "./job-scope/job-scope-technologies-list"
import JobScopeDescription from "./job-scope/job-scope-description"
import JobScopeDates from "./job-scope/job-scope-dates"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grid: {
      marginBottom: "1rem",
    },
  })
)

const JobScopeExpanded = () => {
  const intl = useIntl()
  const { scopeSlug, expandScope } = useJobScope()
  const ref = useRef(null)
  useEffect(() => {
    window.scrollTo(0, ref.current && ref.current.offsetTop)
  }, [scopeSlug])

  const scope = getJobScope(scopeSlug)
  if (!scope) {
    return <JobScopeNotFound slug={scopeSlug} />
  }

  const classes = useStyles({})
  return (
    <div ref={ref}>
      <Flipped flipId={FLIP_ID}>
        <Grid container spacing={3} alignItems="flex-start">
          <Grid item xs={12}>
            <Button
              variant="outlined"
              color="primary"
              onClick={() => expandScope()}
            >
              <KeyboardReturnIcon />
              Back
            </Button>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h5" component="h1">
              {intl.formatMessage({ id: `jobs.${scope.slug}.role.title` })}
            </Typography>
          </Grid>
          <Grid container item xs={12} sm={7}>
            <Grid item xs={12} className={classes.grid}>
              <JobScopeDates scope={scope} />
            </Grid>
            <Grid item xs={12} className={classes.grid}>
              <JobScopeCompany scope={scope} />
            </Grid>
            <Grid item xs={12}>
              <JobScopeDescription scope={scope} />
            </Grid>
          </Grid>
          <Grid container item xs={12} sm={5}>
            <Grid item xs={12}>
              <JobScopeTechnologiesList technologies={scope.technologies} />
            </Grid>
          </Grid>
        </Grid>
      </Flipped>
    </div>
  )
}

export default JobScopeExpanded
