import { useContext } from "react"
import JobScopesContext from "../job-scopes-context"

const useJobScope = () => {
  const { expanded, setExpanded, scopeSlug, setScopeSlug } = useContext(
    JobScopesContext
  )
  const expandScope = (slug: string = null) => {
    setExpanded(!expanded)
    setScopeSlug(slug)
  }
  return {
    expandScope,
    expanded,
    setExpanded,
    scopeSlug,
    setScopeSlug,
  }
}

export default useJobScope
