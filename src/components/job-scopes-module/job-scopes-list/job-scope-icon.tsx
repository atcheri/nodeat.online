import React from "react"
import WebIcon from "@material-ui/icons/Web"
import PhoneAndroidIcon from "@material-ui/icons/PhoneAndroid"
import WorkOutlineIcon from "@material-ui/icons/WorkOutline"
import DesktopWindowsIcon from "@material-ui/icons/DesktopWindows"
import BuildIcon from "@material-ui/icons/Build"

import { SCOPE_TYPE } from "./constants/objects"

interface IJobScopeIconProps {
  type: string
}

const JobScopeIcon = ({ type }: IJobScopeIconProps) => {
  switch (type) {
    case SCOPE_TYPE.WEB:
      return <WebIcon />
    case SCOPE_TYPE.MOBILE:
      return <PhoneAndroidIcon />
    case SCOPE_TYPE.DESKTOP:
      return <DesktopWindowsIcon />
    case SCOPE_TYPE.BUSINESS:
      return <WorkOutlineIcon />
    case SCOPE_TYPE.EXPERIMENT:
      return <BuildIcon />
    default:
      return <WorkOutlineIcon />
  }
}

export default JobScopeIcon
