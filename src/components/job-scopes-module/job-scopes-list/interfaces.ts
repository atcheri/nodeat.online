export interface IIconImage {
  url: string
}

export interface IStyle {
  [key: string]: string
}

export interface ITechnology {
  title: string
  icon: {
    name: string
    style: IStyle
    component?: JSX.Element
    image?: IIconImage
  }
  level: number
  site: string
  description?: string
  repository?: string
}

export interface IAddress {
  city: string
  street: string
  code: string
}

export interface ILocation {
  country: string
  address: IAddress
}

export interface IJobCompany {
  name: string
  sector: string
  location: ILocation
  url?: string
  foundationDate?: Date
  size?: number
}

export interface IJobScope {
  slug: string
  workType: string
  start: Date
  end: Date | null
  company: IJobCompany
  technologies: ITechnology[]
}
