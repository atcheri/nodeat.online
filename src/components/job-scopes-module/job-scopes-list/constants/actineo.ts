import { IJobScope } from "../interfaces"
import { defaultIconStyle, SCOPE_TYPE } from "./objects"

export const actineo: IJobScope = {
  slug: "actineo",
  workType: SCOPE_TYPE.WEB,
  start: new Date("2015-10-01"),
  end: null,
  company: {
    name: "Actineo GMBH",
    sector: "Insuretech",
    location: {
      country: "Germany",
      address: {
        city: "Cologne",
        street: "Mannesmannstraße 5",
        code: "50996",
      },
    },
    foundationDate: new Date("2009"),
  },
  description: "",
  technologies: [
    {
      title: "Nodejs",
      icon: {
        name: "nodejs-plain",
        style: defaultIconStyle,
      },
      level: 4.5,
      site: "https://nodejs.org/",
    },
    {
      title: "Typescript",
      icon: {
        name: "typescript-plain",
        style: defaultIconStyle,
      },
      level: 4.5,
      site: "https://www.typescriptlang.org/",
    },
    {
      title: "Reactjs",
      icon: {
        name: "react-plain",
        style: defaultIconStyle,
      },
      level: 4.5,
      site: "https://reactjs.org/",
    },
    {
      title: "Docker",
      icon: {
        name: "docker-plain",
        style: defaultIconStyle,
      },
      level: 3.5,
      site: "https://www.docker.com/",
    },
    {
      title: "Mongo DB",
      icon: {
        name: "mongodb-plain",
        style: defaultIconStyle,
      },
      level: 4,
      site: "https://www.mongodb.com/",
    },
  ],
}
