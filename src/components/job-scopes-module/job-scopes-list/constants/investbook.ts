import { IJobScope } from "../interfaces"
import { defaultIconStyle, SCOPE_TYPE } from "./objects"

export const investbook: IJobScope = {
  slug: "investbook",
  workType: SCOPE_TYPE.WEB,
  start: new Date("2015-10-01"),
  end: null,
  company: {
    name: "Investbook SAS",
    sector: "Fintech",
    location: {
      country: "France",
      address: {
        city: "Paris",
        street: "64 Rue de la Folie Regnault",
        code: "75011",
      },
    },
    url: "https://www.investbook.fr",
    foundationDate: new Date("2015"),
  },
  technologies: [
    {
      title: "Javascript",
      icon: {
        name: "javascript-plain",
        style: defaultIconStyle,
      },
      level: 5,
      site: "https://www.javascript.com/",
    },
    {
      title: "Vuejs",
      icon: {
        name: "vuejs-plain",
        style: defaultIconStyle,
      },
      level: 4,
      site: "https://vuejs.org/",
    },
    {
      title: "Docker",
      icon: {
        name: "docker-plain",
        style: defaultIconStyle,
      },
      level: 3.5,
      site: "https://www.docker.com/",
    },
    {
      title: "MySQL",
      icon: {
        name: "mysql-plain",
        style: defaultIconStyle,
      },
      level: 3.5,
      site: "https://www.mysql.com/",
    },
    {
      title: "Php",
      icon: {
        name: "php-plain",
        style: defaultIconStyle,
      },
      level: 4.5,
      site: "https://www.php.net/",
    },
    {
      title: "Cake-PHP",
      icon: {
        name: "cakephp-plain",
        style: defaultIconStyle,
        image: {
          url: "https://simpleicons.org/icons/cakephp.svg",
        },
      },
      level: 4,
      site: "https://cakephp.org/",
    },
    {
      title: "Python",
      icon: {
        name: "python-plain",
        style: defaultIconStyle,
      },
      level: 3.5,
      site: "https://www.python.org/",
    },
    {
      title: "JQuery",
      icon: {
        name: "jquery-plain",
        style: defaultIconStyle,
      },
      level: 4.5,
      site: "https://jquery.com/",
    },
  ],
}
