import { IJobScope } from "../interfaces"
import { investbook } from "./investbook"
import { actineo } from "./actineo"
import { tsa } from "./tsa"
import { cfm } from "./cfm"

export const jobScopes: IJobScope[] = [
  { ...actineo },
  { ...investbook },
  { ...tsa },
  { ...cfm },
]

export const getJobScope = (slug?: string) => {
  if (!slug) {
    return null
  }
  return jobScopes.find(s => s.slug === slug)
}
