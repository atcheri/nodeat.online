import { IJobScope } from "../interfaces"
import { defaultIconStyle, SCOPE_TYPE } from "./objects"

export const cfm: IJobScope = {
  slug: "cfm",
  workType: SCOPE_TYPE.BUSINESS,
  start: new Date("2009-10-01"),
  end: new Date("2012-01-02"),
  company: {
    name: "Capital Fund Management",
    sector: "Fintech",
    location: {
      country: "France",
      address: {
        city: "Paris",
        street: "23, rue de l'Université",
        code: "75007",
      },
    },
    url: "https://www.cfm.fr/",
    foundationDate: new Date("1991"),
  },
  technologies: [
    {
      title: "C++",
      icon: {
        name: "cplusplus-plain",
        style: defaultIconStyle,
      },
      level: 3.5,
      site: "http://www.cplusplus.com/",
    },
  ],
}
