export const SCOPE_TYPE = Object.freeze({
  WEB: "web",
  MOBILE: "mobile",
  DESKTOP: "desktop",
  BUSINESS: "business",
  EXPERIMENT: "experiment",
})

export const defaultIconStyle = {
  // fill: "thistle",
  width: "64px",
}
