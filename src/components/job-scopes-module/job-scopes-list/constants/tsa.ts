import { IJobScope } from "../interfaces"
import { defaultIconStyle, SCOPE_TYPE } from "./objects"
import ElasticsearchIcon from "../../../../components/icons/ElasticsearchIcon"

export const tsa: IJobScope = {
  slug: "tsa",
  workType: SCOPE_TYPE.WEB,
  start: new Date("2012-10-01"),
  end: null,
  company: {
    name: "TSA SAS",
    sector: "Tourism",
    location: {
      country: "France",
      address: {
        city: "Paris",
        street: "31 Rue des petits champs",
        code: "75001",
      },
    },
    foundationDate: new Date("2012"),
  },
  technologies: [
    {
      title: "Golang",
      icon: {
        name: "go-line",
        style: defaultIconStyle,
      },
      level: 3.5,
      site: "https://golang.org/",
    },
    {
      title: "Elasticsearch",
      icon: {
        name: "elasticsearch",
        style: defaultIconStyle,
        component: ElasticsearchIcon,
      },
      level: 2.5,
      site: "https://www.elastic.co/",
    },
    {
      title: "MySQL",
      icon: {
        name: "mysql-plain",
        style: defaultIconStyle,
      },
      level: 3.5,
      site: "https://www.mysql.com/",
    },
    {
      title: "Php",
      icon: {
        name: "php-plain",
        style: defaultIconStyle,
      },
      level: 4.5,
      site: "https://www.php.net/",
    },
    {
      title: "Docker",
      icon: {
        name: "docker-plain",
        style: defaultIconStyle,
      },
      level: 3.5,
      site: "https://www.docker.com/",
    },
    {
      title: "Vuejs",
      icon: {
        name: "vuejs-plain",
        style: defaultIconStyle,
      },
      level: 4,
      site: "https://vuejs.org/",
    },
  ],
}
