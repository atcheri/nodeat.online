import { getJobScope } from "../constants"

describe("job-scope-list", () => {
  describe("getJobScope()", () => {
    it("returns null if slug is empty", () => {
      expect(getJobScope()).toBeNull()
    })
    it("returns an object if slug is non a non empty string", () => {
      expect(getJobScope("actin")).not.toBeNull()
    })
  })
})
