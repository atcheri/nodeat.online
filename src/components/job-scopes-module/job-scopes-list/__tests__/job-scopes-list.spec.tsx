import React from "react"
import { render } from "@testing-library/react"
import { IntlProvider } from "react-intl"
import { IntlContextProvider } from "gatsby-plugin-intl"

import { investbook } from "../constants/investbook"
import { IJobScope } from "../interfaces"
import JobScopesList from "../../job-scopes-list"
import JobScopesContext from "../../job-scopes-context"
import { LANGUAGE } from "../../../../constants"

const renderJobScopesList = (items: IJobScope[]) => {
  const value = {
    expanded: false,
    setExpanded: jest.fn(),
    scopeSlug: null,
    setScopeSlug: jest.fn(),
  }
  return render(
    <JobScopesContext.Provider value={value}>
      <IntlContextProvider
        value={{
          languages: Object.values(LANGUAGE),
          language: "en",
          routed: true,
        }}
      >
        <IntlProvider locale={LANGUAGE.ENGLISH} onError={() => null}>
          <JobScopesList items={items} />
        </IntlProvider>
      </IntlContextProvider>
    </JobScopesContext.Provider>
  )
}

describe("<JobScopesList />", () => {
  it("matches snapshot", () => {
    const { container } = renderJobScopesList([])
    expect(container.firstChild).toMatchSnapshot()
  })

  describe("Given 3 job scopes", () => {
    it("contains 3 times the JobScope component", () => {
      const mockItems: IJobScope[] = [investbook, investbook, investbook]
      const { getAllByTestId } = renderJobScopesList(mockItems)
      const items = getAllByTestId("job-scope-item")
      expect(items).toHaveLength(3)
    })
  })
})
