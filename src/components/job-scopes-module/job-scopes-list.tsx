import React, { useCallback } from "react"
import {
  VerticalTimeline,
  VerticalTimelineElement,
} from "react-vertical-timeline-component"
import { useIntl } from "gatsby-plugin-intl"
import "react-vertical-timeline-component/style.min.css"

import JobScope from "./job-scope"
import { IJobScope } from "./job-scopes-list/interfaces"
import JobScopeIcon from "./job-scopes-list/job-scope-icon"

export interface IJobScopesListProps {
  items?: IJobScope[]
}

const JobScopesList = ({ items = [] }: IJobScopesListProps) => {
  const intl = useIntl()

  const fromUntil = useCallback((scope: IJobScope): string => {
    const start = scope.start.getFullYear()
    const end = scope.end
      ? scope.end.getFullYear()
      : intl.formatMessage({ id: "constants.times.today" })
    return `${start} ~ ${end}`
  }, [])

  return (
    <VerticalTimeline layout="1-column">
      {items.map((item: IJobScope, i: number) => {
        return (
          <VerticalTimelineElement
            key={`job-scope-${i}`}
            className="vertical-timeline-element--work"
            date={fromUntil(item)}
            iconStyle={{ background: "rgb(96, 163, 188)", color: "#fff" }}
            icon={<JobScopeIcon type={item.workType} />}
          >
            <JobScope item={item} />
          </VerticalTimelineElement>
        )
      })}
    </VerticalTimeline>
  )
}

export default JobScopesList
