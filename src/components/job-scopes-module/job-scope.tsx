import React from "react"
import { Typography, Box } from "@material-ui/core"
import { Flipped } from "react-flip-toolkit"
import { FormattedMessage } from "gatsby-plugin-intl"
import "devicon/devicon.css"
import "devicon/devicon-colors.css"

import useJobScope from "./hooks/useJobScope"
import { IJobScope } from "./job-scopes-list/interfaces"
import JobScopeTechnologies from "./job-scope/job-scope-technologies"
import { FLIP_ID } from "../job-scopes-module"

import "./job-scope/job-scope.scss"

interface IJobScopeProps {
  item: IJobScope
}

const JobScope = ({ item }: IJobScopeProps) => {
  const { expandScope } = useJobScope()

  return (
    <Flipped flipId={FLIP_ID}>
      <div
        data-testid="job-scope-item"
        className="job-scope-item"
        onClick={() => expandScope(item.slug)}
      >
        <Typography className="capitalize" variant="body1" component="h2">
          <FormattedMessage id={`jobs.${item.slug}.role.title`} />
        </Typography>
        <Typography component="div" color="textSecondary">
          {`${item.company.location.address.city}, `}
          <FormattedMessage
            id={`constants.countries.${item.company.location.country}.name`}
          />
        </Typography>
        <Box component="span" display="block" css={{ marginY: "10px" }} />
        <Typography className="capitalize" variant="subtitle1" component="h3">
          {item.company.name}
        </Typography>
        <Typography component="div" color="textSecondary">
          <FormattedMessage id={`jobs.${item.slug}.company.punchline`} />
        </Typography>
        <Typography
          gutterBottom
          variant="body2"
          color="textSecondary"
          component="div"
        >
          <Box component="span" display="block" css={{ marginY: "10px" }} />
          <Typography variant="body2" component="div">
            Technologies:
          </Typography>
          <JobScopeTechnologies technologies={item.technologies} />
        </Typography>
      </div>
    </Flipped>
  )
}

export default JobScope
