import React from "react"
import { render } from "@testing-library/react"
import { IntlProvider } from "react-intl"
import { IntlContextProvider } from "gatsby-plugin-intl"

import JobScopeExpanded from "../job-scope-expanded"
import JobScopesContext from "../job-scopes-context"
import { LANGUAGE } from "../../../constants"

const spyScrollTo = jest.fn()

describe("<JobScopeExpanded />", () => {
  describe("Given no found job scope ", () => {
    beforeEach(() => {
      Object.defineProperty(global.window, "scrollTo", { value: spyScrollTo })
      Object.defineProperty(global.window, "scrollY", { value: 1 })
      spyScrollTo.mockClear()
    })
    it("shows <JobScopeNotFound /> component", () => {
      const value = {
        expanded: false,
        setExpanded: jest.fn(),
        scopeSlug: null,
        setScopeSlug: jest.fn(),
      }
      const { getByTestId } = render(
        <JobScopesContext.Provider value={value}>
          <IntlContextProvider
            value={{
              languages: Object.values(LANGUAGE),
              language: "en",
              routed: true,
            }}
          >
            <IntlProvider locale={LANGUAGE.ENGLISH} onError={() => null}>
              <JobScopeExpanded />
            </IntlProvider>
          </IntlContextProvider>
        </JobScopesContext.Provider>
      )
      getByTestId("job-scope-not-found")
      expect(spyScrollTo).toHaveBeenCalled()
    })
  })
})
