import React from "react"
import {
  Button,
  Paper,
  Typography,
  Input,
  FormControl,
  InputLabel,
  CircularProgress,
  Box,
} from "@material-ui/core"
import SendIcon from "@material-ui/icons/Send"
import CheckIcon from "@material-ui/icons/Check"

import { ValidatorForm, TextValidator } from "react-material-ui-form-validator"
import MaskedInput from "react-text-mask"
import AniLink from "gatsby-plugin-transition-link/AniLink"
import { Theme, withStyles } from "@material-ui/core/styles"
import { teal } from "@material-ui/core/colors"

import ContactFormDialog from "./contact-form/contact-form-dialog"
import { sendToMailerApi } from "../api/mailerApi"

import "./contact-form/contact-form.scss"

interface PhoneMaskPProps {
  inputRef: (ref: HTMLInputElement | null) => void
}

const PhoneMask = (props: PhoneMaskPProps) => {
  const { inputRef, ...other } = props

  return (
    <MaskedInput
      {...other}
      ref={(ref: any) => {
        inputRef(ref ? ref.inputElement : null)
      }}
      mask={[
        /\d/,
        /\d/,
        /\d/,
        /\d/,
        "-",
        /\d/,
        /\d/,
        /\d/,
        /\d/,
        "-",
        /\d/,
        /\d/,
        /\d/,
        /\d/,
        "-",
        /\d/,
        /\d/,
        /\d/,
        /\d/,
      ]}
      placeholderChar={"\u2000"}
      showMask
    />
  )
}

const RelativeBox = withStyles((theme: Theme) => ({
  root: { margin: theme.spacing(1), position: "relative" },
}))(Box)

const ColorButton = withStyles((theme: Theme) => ({
  root: {
    color: theme.palette.getContrastText(teal[500]),
    backgroundColor: "#34cc99",
    "&:hover": {
      backgroundColor: "#1c6f53",
    },
  },
}))(Button)

const CircularButtonLoader = withStyles((theme: Theme) => ({
  root: {
    color: "#ffffff",
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
}))(CircularProgress)

export const CONTACT_FORM = "contact-form"
export const CONTACT_FORM_SUBMIT_BTN = "contact-form-submit-btn"

class ContactForm extends React.Component {
  state = {
    open: false,
    modalType: "",
    loading: false,
    name: "",
    email: "",
    phone: "",
    message: "",
  }

  handleChange = event => {
    const { name, value } = event.target

    this.setState({ [name]: value })
  }

  handleSubmit = async (e: Event) => {
    e.preventDefault()
    this.setState({ loading: true })
    const { name, email, phone, message } = this.state
    try {
      const sent = await sendToMailerApi({ name, email, phone, message })
      sent
        ? this.setState({ open: true, modalType: "success" })
        : this.setState({ open: true, modalType: "error" })
    } catch (err) {
      console.log(err)
    }
    this.setState({ open: false, loading: false, modalType: "" })
  }

  handleErrors = errors => {
    // console.log(errors)
  }

  handleModalState = () => {
    this.setState({ open: !this.state.open })
  }

  render() {
    const { open, modalType, loading, name, email, phone, message } = this.state
    const success = modalType === "success"
    return (
      <>
        <Paper className={CONTACT_FORM} data-testid={CONTACT_FORM}>
          <Typography className="header" variant="h5" component="h1">
            if (this.webSite.nothingInterstingFound()){" { "}
            <br />{" "}
            {
              <AniLink
                cover
                bg="#34cc99"
                to="/"
                style={{ textDecoration: "none", paddingLeft: "2rem" }}
              >
                <Button variant="outlined" size="medium" color="primary">
                  return home;
                </Button>
              </AniLink>
            }
            <br />
            {" } "}
          </Typography>
          <Typography className="header" variant="h5" component="p">
            this.webSite.submitContactForm();
          </Typography>
          <ValidatorForm
            ref="form"
            onSubmit={this.handleSubmit}
            onError={this.handleErrors}
          >
            <TextValidator
              label="Your name"
              className="fill-width-input"
              onChange={this.handleChange}
              name="name"
              value={name}
              validators={["required"]}
              errorMessages={["Name field is required"]}
            />
            <TextValidator
              label="Email"
              className="fill-width-input"
              onChange={this.handleChange}
              name="email"
              value={email}
              validators={["required", "isEmail"]}
              errorMessages={["Email field is required", "Email is not valid"]}
            />
            <FormControl className="fill-width-input">
              <InputLabel htmlFor="contact-phone-number">
                Phone number
              </InputLabel>
              <Input
                onChange={this.handleChange}
                name="phone"
                value={phone}
                id="contact-phone-number"
                inputComponent={PhoneMask as any}
              />
            </FormControl>
            <TextValidator
              label="Message"
              className="fill-width-input"
              onChange={this.handleChange}
              name="message"
              value={message}
              validators={["required"]}
              errorMessages={["Message field is required"]}
              multiline
              rows="4"
            />
            <Box
              style={{
                paddingTop: "1rem",
                display: "flex",
                alignItems: "center",
              }}
            >
              <RelativeBox>
                <ColorButton
                  className="contact-submit-button"
                  type="submit"
                  disabled={loading}
                  data-testid={CONTACT_FORM_SUBMIT_BTN}
                >
                  {success ? (
                    <CheckIcon />
                  ) : (
                    <>
                      <SendIcon className="icon" />
                      Send()
                    </>
                  )}
                </ColorButton>
                {loading && <CircularButtonLoader size={24} />}
              </RelativeBox>
            </Box>
          </ValidatorForm>
        </Paper>
        {open && (
          <ContactFormDialog
            open={open}
            setOpen={this.handleModalState}
            type={modalType}
          />
        )}
      </>
    )
  }
}

export default ContactForm
