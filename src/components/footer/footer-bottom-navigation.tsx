import React from "react"
import { navigate, Location } from "@reach/router"
import { BottomNavigation, BottomNavigationAction } from "@material-ui/core"
import MoodIcon from "@material-ui/icons/Mood"

import { PAGE_NAMES } from "../../constants"

const NAVIGATION_OBJECTS = Object.freeze({
  JOKE: {
    LABEL: "Joke",
    PATH: PAGE_NAMES.JOKE_PAGE,
    ICON: <MoodIcon />,
  },

  JOB: {
    LABEL: "Projects",
    PATH: PAGE_NAMES.JOB_SCOPES,
    ICON: <MoodIcon />,
  },
  CONTACT: {
    LABEL: "Contact",
    PATH: PAGE_NAMES.CONTACT,
    ICON: <MoodIcon />,
  },
})

const FooterBottomNavigation = () => {
  return (
    <Location>
      {({ location }) => {
        const value = location.pathname.slice(1)
        return (
          <BottomNavigation value={value}>
            {Object.entries(NAVIGATION_OBJECTS).map(([, value], o) => {
              return (
                <BottomNavigationAction
                  key={o}
                  label={value.LABEL}
                  value={value.PATH}
                  icon={value.ICON}
                  showLabel
                />
              )
            })}
          </BottomNavigation>
        )
      }}
    </Location>
  )
}

export default FooterBottomNavigation
