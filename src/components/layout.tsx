/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { injectIntl } from "gatsby-plugin-intl"

import Header from "./header"
import Footer from "./footer"
import "./layout.css"

const Layout = ({ children, intl }) => (
  <>
    <Header siteTitle={intl.formatMessage({ id: "site.title" })} />
    <div
      style={{
        margin: `0 auto`,
        maxWidth: 960,
        padding: `0px 0rem 1.45rem`,
        paddingTop: 0,
      }}
    >
      <main>{children}</main>
    </div>
    <Footer />
  </>
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default injectIntl(Layout)
