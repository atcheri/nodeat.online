import React from "react"
import { render, wait, act } from "@testing-library/react"
import Footer from "../footer"

const resizeWindow = (x: number, y: number = 480) => {
  window.innerWidth = x
  window.innerHeight = y
  window.dispatchEvent(new Event("resize"))
}

describe("<Footer />", () => {
  afterEach(() => {
    jest.clearAllMocks()
  })
  describe("When the window is bigger than 600px", () => {
    it("shows the bottom part", async () => {
      const { getByTestId } = render(<Footer />)
      await wait(() => getByTestId("footer-bottom"))
    })
  })
  describe.skip("When the window is resized to a width less than 600px", () => {
    it("hides the bottom part", () => {
      const { queryByTestId, rerender } = render(<Footer />)
      act(() => {
        const smallSize = 600 - 1
        resizeWindow(smallSize)
      })
      rerender(<Footer />)
      expect(queryByTestId("footer-bottom")).toBeNull()
    })
  })
})
