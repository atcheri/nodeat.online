import React, { useState, useEffect, useRef } from "react"
import {
  List,
  ListItem,
  ListItemText,
  Typography,
  ListItemIcon,
  IconButton,
  Dialog,
  DialogTitle,
  DialogContent,
} from "@material-ui/core"
import ThumbsUpDownOutlinedIcon from "@material-ui/icons/ThumbsUpDownOutlined"
import { spring } from "react-flip-toolkit"

import { IJoke, JOKE_API_URL } from "../../api/jokeApi"

import "./joke-list/joke-list.scss"
import JokeItemText from "./joke-list/joke-item-text"

export interface IJokesListProps {
  jokes: IJoke[]
}

const JokesList = ({ jokes = [] }: IJokesListProps) => {
  const [open, setOpen] = useState(false)
  const containerRef = useRef(null)
  useEffect(() => {
    const jokeElements = [
      ...containerRef.current.querySelectorAll(".joke-list-item"),
    ]
    jokeElements.forEach((el, i) => {
      spring({
        config: "wobbly",
        values: {
          translateY: [25, 0],
          opacity: [0, 1],
        },
        onUpdate: ({ translateY, opacity }: Record<string, number>) => {
          el.style.opacity = opacity
          el.style.transform = `translateY(${translateY}px)`
        },
        delay: i * 50,
      })
    })
  }, [jokes])

  return (
    <>
      <Dialog
        data-testid="up-down-vote-dialog"
        open={open}
        onClose={() => setOpen(!open)}
      >
        <DialogTitle id="simple-dialog-title">
          Thank you for your feedback!
        </DialogTitle>
        <DialogContent>
          You've voted up and down at the same time. How is that possible !?
        </DialogContent>
      </Dialog>

      <List data-testid="jokes-list" ref={containerRef}>
        {jokes.map(({ id, joke }) => (
          <ListItem key={id} alignItems="flex-start" className="joke-list-item">
            <ListItemIcon onClick={() => setOpen(!open)}>
              <IconButton data-testid="thumbs-up-down-icon">
                <ThumbsUpDownOutlinedIcon color="primary" />
              </IconButton>
            </ListItemIcon>
            <ListItemText
              primary={
                <Typography
                  component="span"
                  variant="body2"
                  color="textPrimary"
                >
                  Joke ID:{" "}
                  <a
                    href={`${JOKE_API_URL}j/${id}`}
                    target="_blank"
                    rel="nofollow"
                  >
                    {id}
                  </a>
                </Typography>
              }
              secondary={<JokeItemText joke={joke} />}
            />
          </ListItem>
        ))}
      </List>
    </>
  )
}

export default JokesList
