import React, { FunctionComponent } from "react"
import { Typography } from "@material-ui/core"

export const NO_JOKE_FOUND = "no-joke-found"

interface INoJokeFoundProps {
  searched: boolean
  searchValue: string
}

const NoJokeFound: FunctionComponent<INoJokeFoundProps> = ({
  searched,
  searchValue,
}) => {
  return (
    searched && (
      <Typography
        component="p"
        variant="body2"
        color="textPrimary"
        style={{ marginTop: "1rem" }}
        data-testid={NO_JOKE_FOUND}
      >
        No result found for the entered keyword(s): <b>{searchValue}</b>
      </Typography>
    )
  )
}

export default NoJokeFound
