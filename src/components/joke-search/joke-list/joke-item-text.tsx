import React, { FunctionComponent, useState, useEffect } from "react"
import { useIntl } from "gatsby-plugin-intl"

import { LANGUAGE } from "../../../constants"
import { translateText } from "../../../api/translationApi"
import { getLanguageName } from "../../header/helpers"

interface IJokeItemText {
  joke: string
}

const JokeItemText: FunctionComponent<IJokeItemText> = ({ joke }) => {
  const { locale } = useIntl()
  const [translation, setTranslation] = useState(null)
  const isEnglish = locale === LANGUAGE.ENGLISH
  useEffect(() => {
    if (!isEnglish) {
      translateText(joke, "en", locale)
        .then(trans => setTranslation(trans))
        .catch(console.log)
    }
  }, [locale])

  return (
    <>
      <span> - {joke}</span>
      {!isEnglish && (
        <>
          <br />
          <i>
            {getLanguageName(locale)}: {translation}
          </i>
        </>
      )}
    </>
  )
}

export default JokeItemText
