import React from "react"
import { act, render, fireEvent, wait } from "@testing-library/react"
import { IntlProvider } from "react-intl"
import { IntlContextProvider } from "gatsby-plugin-intl"

import JokeSearch from "../../joke-search"
import * as helpers from "../helpers"
import * as jokeApi from "../../../api/jokeApi"
import { NO_JOKE_FOUND } from "../node-joke-found"
import { LANGUAGE } from "../../../constants"

jest.mock("gatsby-plugin-transition-link/AniLink", () => {
  return ({ children }) => <>{children}</>
})

const renderComponent = (lang: string = LANGUAGE.ENGLISH) =>
  render(
    <IntlContextProvider
      value={{
        languages: Object.values(LANGUAGE),
        language: lang,
        routed: true,
      }}
    >
      <IntlProvider locale={lang} onError={() => null}>
        <JokeSearch />
      </IntlProvider>
    </IntlContextProvider>
  )

describe("<JokeSearch />", () => {
  it("should find the joke-search-input", () => {
    const { getByTestId } = renderComponent()
    getByTestId("joke-search-input")
  })

  describe("Mood <IconButton />", () => {
    it('Should call the "generateZeroOrOne" function when the "mood-icon" is clicked', () => {
      const spy = jest
        .spyOn(helpers, "generateZeroOrOne")
        .mockImplementation(() => 0)
      const { getByTestId } = renderComponent()
      const iconBtn = getByTestId("mood-icon")
      fireEvent.click(iconBtn)
      expect(spy).toHaveBeenCalled()
    })
  })

  describe("Search <IconButton />", () => {
    it("Should disable the search button if input value is empty", () => {
      const { getByTestId } = renderComponent()
      const btn = getByTestId("joke-search-button")
      expect(btn).toBeDisabled()
    })
    describe("When input is not empty", () => {
      it("Should enable the search button if input has more than 2 characters", () => {
        const { getByTestId } = renderComponent()
        const input = getByTestId("joke-search-input")
        fireEvent.change(input, { target: { value: "aaa" } })
        const btn = getByTestId("joke-search-button")
        expect(btn).toBeEnabled()
      })
      it('Should call the "fetchFromJokeApi" function when search button is clicked', async () => {
        const spy = jest
          .spyOn(jokeApi, "fetchFromJokeApi")
          .mockImplementation(() => Promise.resolve([]))
        const { getByTestId } = renderComponent()
        const input = getByTestId("joke-search-input")
        const submitBtn = getByTestId("joke-search-button")
        await act(async () => {
          await wait(() =>
            fireEvent.change(input, { target: { value: "aaa" } })
          )
          fireEvent.click(submitBtn)
        })
        expect(spy).toHaveBeenCalled()
      })
    })
  })

  it("Doesn't show the list of jokes if no jokes have been found", () => {
    const { queryByTestId } = renderComponent()
    expect(queryByTestId("jokes-list")).toBeNull()
  })

  it("Shows the list of jokes if jokes have been found", async () => {
    const spy = jest.spyOn(jokeApi, "fetchFromJokeApi")
    spy.mockReturnValue(
      Promise.resolve([
        {
          id: "test-id-1",
          joke: "this is a funny joke",
          status: 200,
        },
        {
          id: "test-id-2",
          joke: "this is not a funny joke",
          status: 200,
        },
      ])
    )

    const { getByTestId } = renderComponent()
    const input = getByTestId("joke-search-input")
    const submitBtn = getByTestId("joke-search-button")
    await act(async () => {
      await wait(() => fireEvent.change(input, { target: { value: "aaa" } }))
      fireEvent.click(submitBtn)
    })
    getByTestId("jokes-list")
  })

  it("Displays <NoJokeFound /> component when no jokes have been found", async () => {
    const spy = jest.spyOn(jokeApi, "fetchFromJokeApi")
    spy.mockReturnValue(Promise.resolve([]))

    const { queryByTestId, getByTestId } = renderComponent()
    const input = getByTestId("joke-search-input")
    const submitBtn = getByTestId("joke-search-button")
    await act(async () => {
      await wait(() => fireEvent.change(input, { target: { value: "aaa" } }))
      fireEvent.click(submitBtn)
    })
    expect(queryByTestId("jokes-list")).toBeNull()
    getByTestId(NO_JOKE_FOUND)
  })
})
