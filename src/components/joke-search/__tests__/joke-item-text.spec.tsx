import React from "react"
import { render, wait } from "@testing-library/react"
import { IntlProvider } from "react-intl"
import { IntlContextProvider } from "gatsby-plugin-intl"

import JokeItemText from "../joke-list/joke-item-text"
import * as translationApi from "../../../api/translationApi"
import { LANGUAGE } from "../../../constants"

const joke = "this is a funny joke"

const renderComponent = (lang: string = LANGUAGE.ENGLISH) =>
  render(
    <IntlContextProvider
      value={{
        languages: Object.values(LANGUAGE),
        language: lang,
        routed: true,
      }}
    >
      <IntlProvider locale={lang} onError={() => null}>
        <JokeItemText joke={joke} />
      </IntlProvider>
    </IntlContextProvider>
  )

describe("<JokeItemText />", () => {
  describe('When the chosen language is "en"', () => {
    it("Doesn't Call the google translation API", () => {
      const spy = jest
        .spyOn(translationApi, "translateText")
        .mockImplementationOnce(async () => "blague non traduite")
      renderComponent()
      expect(spy).not.toHaveBeenCalled()
    })
  })
  describe('When the chosen language is different to "en"', () => {
    it("Calls the google translation API", async () => {
      const spy = jest
        .spyOn(translationApi, "translateText")
        .mockImplementationOnce(async () => "blague traduite")
      renderComponent(LANGUAGE.FRENCH)
      await wait()
      expect(spy).toHaveBeenCalled()
    })
  })
})
