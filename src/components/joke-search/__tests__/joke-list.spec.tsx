import React from "react"
import { render, fireEvent, waitForElement } from "@testing-library/react"
import { IntlProvider } from "react-intl"

import JokeList from "../jokes-list"
import { LANGUAGE } from "../../../constants"

const jokes = [
  {
    id: "test-id-1",
    joke: "this is a funny joke",
    status: 200,
  },
  {
    id: "test-id-2",
    joke: "this is not a funny joke",
    status: 200,
  },
]

const renderComponent = () =>
  render(
    <IntlProvider locale={LANGUAGE.ENGLISH} onError={() => null}>
      <JokeList jokes={jokes} />
    </IntlProvider>
  )

describe("<JokeList />", () => {
  it("Should use material-ui List components and  match snapshot", () => {
    const { asFragment } = renderComponent()
    expect(asFragment()).toMatchSnapshot()
  })
  it("Should open the <Dialog /> when the ThumbsUpDownOutlinedIcon icon is clicked", async () => {
    const { getAllByTestId, getByTestId } = renderComponent()
    const icons = getAllByTestId("thumbs-up-down-icon")
    const firstIcon = icons.pop()
    fireEvent.click(firstIcon)
    const dialog = await waitForElement(() =>
      getByTestId("up-down-vote-dialog")
    )
    expect(dialog).toBeTruthy()
  })
})
