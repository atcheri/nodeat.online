import React, { useContext } from "react"
import { Flipper } from "react-flip-toolkit"

import JobScopeExpanded from "./job-scopes-module/job-scope-expanded"
import JobScopesList from "./job-scopes-module/job-scopes-list"
import { jobScopes } from "./job-scopes-module/job-scopes-list/constants"
import JobScopesContext from "./job-scopes-module/job-scopes-context"

export const FLIP_ID = "JOB_SCOPE"

const JobScopesModule = () => {
  const { expanded } = useContext(JobScopesContext)

  return (
    <Flipper flipKey={expanded}>
      {expanded ? <JobScopeExpanded /> : <JobScopesList items={jobScopes} />}
    </Flipper>
  )
}

export default JobScopesModule
