import React, { FunctionComponent } from "react"
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  Box,
} from "@material-ui/core"
import { green, orange } from "@material-ui/core/colors"
import AniLink from "gatsby-plugin-transition-link/AniLink"
import Grow from "@material-ui/core/Grow"
import { TransitionProps } from "@material-ui/core/transitions"
import CheckIcon from "@material-ui/icons/Check"
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline"

import { PAGE_NAMES } from "../../constants"

interface IContactFormDialog {
  open: boolean
  setOpen(boolean): void
  type: string
}

const Transition = React.forwardRef<unknown, TransitionProps>(
  function Transition(props, ref) {
    return <Grow in ref={ref} {...props} />
  }
)

const ContactFormDialog: FunctionComponent<IContactFormDialog> = ({
  open,
  setOpen,
  type,
}) => {
  const handleClose = () => {
    setOpen(false)
  }

  const displayMessage = (t: string) => {
    return t === "success" ? (
      <Box style={{ textAlign: "center" }}>
        <CheckIcon fontSize="large" style={{ color: green[500] }} />
        <Box component="p">Thank you</Box>
        <Box component="p">Your message has succesfully been sent!</Box>
      </Box>
    ) : (
      <Box style={{ textAlign: "center" }}>
        <ErrorOutlineIcon fontSize="large" style={{ color: orange[500] }} />
        <Box component="p">...</Box>
        <Box component="p">There was a problem sending your message</Box>
        <Box component="p">Please, try again in couple of seconds</Box>
      </Box>
    )
  }

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      onClose={handleClose}
      aria-labelledby="alert-dialog-grow-title"
      aria-describedby="alert-dialog-grow-description"
    >
      <DialogTitle id="alert-dialog-grow-title">
        {displayMessage(type)}
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-grow-description"></DialogContentText>
      </DialogContent>
      <DialogActions>
        {type === "success" ? (
          <AniLink cover bg="#34cc99" to={`/${PAGE_NAMES.JOB_SCOPES}`}>
            <Button onClick={handleClose} color="primary">
              To Job page
            </Button>
          </AniLink>
        ) : (
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
        )}
      </DialogActions>
    </Dialog>
  )
}

export default ContactFormDialog
