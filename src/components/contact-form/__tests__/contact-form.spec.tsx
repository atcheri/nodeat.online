import React from "react"
import {
  render,
  fireEvent,
  waitForDomChange,
  wait,
} from "@testing-library/react"

import * as mailerApi from "../../../api/mailerApi"
import ContactForm from "../../contact-form"
import { CONTACT_FORM_SUBMIT_BTN } from "../../contact-form"

jest.mock("gatsby-plugin-transition-link/AniLink", () => {
  return ({ children }) => <>{children}</>
})

describe("<ContactForm />", () => {
  describe("Given required fields not being all filled", () => {
    it("doesn't call the sendToMailerApi function", async () => {
      const spy = jest
        .spyOn(mailerApi, "sendToMailerApi")
        .mockImplementationOnce(() => {
          return Promise.resolve(true)
        })
      const { getByTestId } = render(<ContactForm />)
      const button = getByTestId(CONTACT_FORM_SUBMIT_BTN)
      fireEvent.click(button)

      await waitForDomChange()
      expect(spy).not.toBeCalled()
    })
  })

  describe("Given required fields all filled", () => {
    it("calls the sendToMailerApi function with the payload", async done => {
      const spy = jest
        .spyOn(mailerApi, "sendToMailerApi")
        .mockImplementationOnce(() => Promise.resolve(true))
      const { getByTestId, container } = render(<ContactForm />)

      const payload = {
        name: "Firstname and Lastname",
        email: "personal@email-address.com",
        phone: "0145-1234-5678-9123",
        message: "This is a super long lorem ipsum message",
      }
      fireEvent.change(container.querySelector('[name="name"]'), {
        target: { value: payload.name },
      })
      fireEvent.change(container.querySelector('[name="email"]'), {
        target: { value: payload.email },
      })
      fireEvent.change(container.querySelector('[name="phone"]'), {
        target: { value: payload.phone },
      })
      fireEvent.change(container.querySelector('[name="message"]'), {
        target: { value: payload.message },
      })

      const button = getByTestId(CONTACT_FORM_SUBMIT_BTN)
      fireEvent.click(button)
      await wait(() => {
        expect(spy).toHaveBeenCalledWith(payload)
        done()
      })
    })
  })
})
