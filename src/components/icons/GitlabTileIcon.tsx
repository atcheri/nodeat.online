import React, { FunctionComponent } from "react"
import { SvgIcon, SvgIconProps } from "@material-ui/core"

import GitlabTileSvg from "../../images/svg/gitlab-tile.svg"

const GitlabTileIcon: FunctionComponent<SvgIconProps> = props => {
  return <SvgIcon viewBox="0 0 512 512" {...props} component={GitlabTileSvg} />
}

export default GitlabTileIcon
