import React, { FunctionComponent } from "react"
import { SvgIcon, SvgIconProps } from "@material-ui/core"

import GitlabSvg from "../../images/svg/gitlab-icon.svg"

const GitlabIcon: FunctionComponent<SvgIconProps> = props => {
  return <SvgIcon viewBox="0 0 64 64" {...props} component={GitlabSvg} />
}

export default GitlabIcon
