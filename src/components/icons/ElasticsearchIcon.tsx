import React, { FunctionComponent } from "react"
import { SvgIcon, SvgIconProps } from "@material-ui/core"

import ElasticsearchSvg from "../../images/svg/elasticsearch.svg"

const ElasticsearchIcon: FunctionComponent<SvgIconProps> = props => {
  return (
    <SvgIcon viewBox="0 0 256 256" {...props} component={ElasticsearchSvg} />
  )
}

export default ElasticsearchIcon
