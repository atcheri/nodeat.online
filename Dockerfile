FROM node:12.14.0-alpine3.11 AS builder

WORKDIR /app

COPY gatsby-* ./
COPY package.json ./
COPY src ./src

RUN yarn --no-lockfile --silent
RUN yarn build